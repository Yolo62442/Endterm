package main

import (
	"bytes"
	"io"
	"log"
	"os"
)

func read(ch chan []byte)  {
	var records []*record
	for node := range ch {
		found := false
		for i, v := range records {
			if bytes.Equal(v.word, node) {
				records[i].counter++
				found = true
				break
			}
		}
		if !found {
			records = append(records, &record{
				word:    node,
				counter: 1,
				checked: false,
			})
		}
	}
	list := make([]int, 20)
	for index, _ := range list {
		temp := 0
		inss := 0
		for index, v := range records {
			if (v.checked == false) && (v.counter > temp) {
				temp = v.counter
				inss = index
			}
		}
		records[inss].checked = true
		list[index] = inss
	}
}
func FastSearch(out io.Writer) {

	file, err := os.Open("mobydick.txt") //open file
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err = file.Close(); err != nil {
			log.Fatal(err)
		}
	}()
	readingBuf := make([]byte, 1)

	writingBuf := make([]byte, 0)

	ch := make(chan []byte) //channel that we will use to pass slices of bytes from writer to reader
	//btw reader listens in range of elements that are passed to channel, it will stop working when there are no elements left, so we don't need any wait groups

	go func() {
		for {
			//reading file's letters one by one
			n, err := file.Read(readingBuf)

			if n > 0 {
				byteVal := readingBuf[0]
				if byteVal >= 65 && byteVal <= 90 { //if symbol is uppercase letter
					byteVal = byteVal + 32 //writing to temporary buffer
					writingBuf = append(writingBuf, byteVal)
				} else if byteVal >= 97 && byteVal <= 122 {
					writingBuf = append(writingBuf, byteVal)

				} else if len(writingBuf) != 0 { //if symbol is [space], and we have letters in our buffer
					ch <- writingBuf
					writingBuf = []byte{}//send temporary buffer content to channel, empty the temporary buffer

				}
			}
			if err == io.EOF {
				ch <- writingBuf //send temporary buffer content to channel, empty the temporary buffer
				break
			}
		}
		close(ch) //close channel, so our that our reader will stop working after there are no elements left, in other case reader will cause deadlock
	}()

	 read(ch)//reading from channel in range of elements in channel
}
